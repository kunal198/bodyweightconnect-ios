//
//  ImprintTermsPrivacy.m
//  BodyweightConnect
//
//  Created by mrinal khullar on 1/11/17.
//  Copyright © 2017 Augment Deck Technologies. All rights reserved.
//

#import "ImprintTermsPrivacy.h"

@interface ImprintTermsPrivacy ()

@end

@implementation ImprintTermsPrivacy

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
[self.view setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0.1]];
    NSLog(@"%@",self.uniquenumber);
    NSString *imprinturl=@"https://bodyweight-connect.com/imprint.html";
    NSString *termsurl=@"https://bodyweight-connect.com/terms.html";
    NSString *privacyurl=@"https://bodyweight-connect.com/privacy.html";
    
    if([self.uniquenumber  isEqual: @"imprint"])
    {
        NSURLRequest *newRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:imprinturl]];
        [self.webView loadRequest:newRequest];
        self.titleLabel.text = @"IMPRINT";
    }
    else if([self.uniquenumber  isEqual: @"terms"])
    {
        NSURLRequest *newRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:termsurl]];
        [self.webView loadRequest:newRequest];
        
        self.titleLabel.text = @"TERMS";
    }
    else
    {
        NSURLRequest *newRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:privacyurl]];
        [self.webView loadRequest:newRequest];
        
        self.titleLabel.text = @"PRIVACY";
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeAction:(UIButton *)sender
{
    [self dismissViewControllerAnimated:false completion:nil];
}
@end
