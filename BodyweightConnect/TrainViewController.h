//
//  TrainViewController.h
//  BodyweightConnect
//
//  Created by Augment Deck Technologies on 14/12/16.
//  Copyright © 2016 Augment Deck Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrainViewController : UIViewController
{
    UITapGestureRecognizer *letterTapRecognizer;
    
    NSMutableArray *arrayForSportsName;
}
@property (strong, nonatomic) IBOutlet UIView *menuView;
- (IBAction)menuButton:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *blurView;


@property (strong, nonatomic) IBOutlet UIView *viewToShowWebViewForTermsNPrivacy;
@property (strong, nonatomic) IBOutlet UIWebView *webViewTerms;
@property (strong, nonatomic) IBOutlet UILabel *titleLabelTerms;
@property (strong, nonatomic) IBOutlet UILabel *demolabel;
@property (strong, nonatomic) IBOutlet UITextView *demotextview;
@property (strong, nonatomic) IBOutlet UIImageView *flicImage;
@property (strong, nonatomic) IBOutlet UIButton *phonebutton;
@property (strong, nonatomic) IBOutlet UIButton *flicButton;
@property (strong, nonatomic) IBOutlet UITableView *sportsTableView;

- (IBAction)imprintAction:(UIButton *)sender;
- (IBAction)termsAction:(UIButton *)sender;
- (IBAction)privacyAction:(UIButton *)sender;
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)settingsAction:(UIButton *)sender;
- (IBAction)closeButton:(UIButton *)sender;
- (IBAction)AddPageAction:(UIButton *)sender;
- (IBAction)phoneSelectionAction:(UIButton *)sender;
- (IBAction)flicSelectionAction:(UIButton *)sender;
@end
