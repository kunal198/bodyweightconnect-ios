//
//  ImprintTermsPrivacy.h
//  BodyweightConnect
//
//  Created by mrinal khullar on 1/11/17.
//  Copyright © 2017 Augment Deck Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImprintTermsPrivacy : UIViewController
- (IBAction)closeAction:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property(nonatomic,strong)NSString *uniquenumber;
@end
