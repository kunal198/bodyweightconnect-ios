//
//  SportsTableViewcell.h
//  BodyweightConnect
//
//  Created by mrinal khullar on 1/13/17.
//  Copyright © 2017 Augment Deck Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SportsTableViewcell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UIButton *button1;

@end
