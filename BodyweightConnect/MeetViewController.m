//
//  MeetViewController.m
//  BodyweightConnect
//
//  Created by Augment Deck Technologies on 14/12/16.
//  Copyright © 2016 Augment Deck Technologies. All rights reserved.
//

#import "MeetViewController.h"
#import <MapKit/MapKit.h>
#import "LFHeatMap.h"
#import "ImprintTermsPrivacy.h"
#import "SettingsVC.h"
#import "MeetTableViewCell.h"

//#import <GoogleMaps/GoogleMaps.h>
//#import <CoreLocation/CoreLocation.h>
@interface MeetViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) NSMutableArray *locations;
@property (nonatomic) NSMutableArray *weights;
@end

@implementation MeetViewController
static NSString *const kLatitude = @"latitude";
static NSString *const kLongitude = @"longitude";
static NSString *const kMagnitude = @"magnitude";

@synthesize Lat,Long,MapViewios;
- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title=@"MEET";
    MapViewios.delegate = self;
      MapViewios.showsUserLocation = YES;
    // mapView=[[GMSMapView alloc]initWithFrame:CGRectMake(0, 0, 320, 568)];
    // [self CreateOverlay];
   
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
//NSLog(@"%@", [self deviceLocation]);
    
    [locationManager requestWhenInUseAuthorization];
    [locationManager requestAlwaysAuthorization];
    CLLocation *location = [locationManager location];
    
    // Configure the new event with information from the location
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    Lat = [NSString stringWithFormat:@"%f", coordinate.latitude];
    Long = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
   // [self registerWebService];
    //NSLog(@"%@",latitude);
    //NSLog(@"%@",longitude);

    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[Lat doubleValue]
                                                            longitude:[Long doubleValue]
                                                                 zoom:5];
   // CLLocationCoordinate2D myLocation = mapView.myLocation.coordinate;
    
    //_mapView = [GMSMapView mapWithFrame:self.googleMapView.frame camera:camera];
    _mapView.camera = camera;
    _mapView.mapType = kGMSTypeNormal;
    [_mapView addObserver:self
                   forKeyPath:@"myLocation"
                      options:(NSKeyValueObservingOptionNew |
                               NSKeyValueObservingOptionOld)
                      context:NULL];;
    _mapView.myLocationEnabled = YES;
    _mapView.mapType = kGMSTypeNormal;
    _mapView.accessibilityElementsHidden = NO;
    _mapView.settings.scrollGestures = YES;
    _mapView.settings.zoomGestures = YES;
    _mapView.settings.compassButton = YES;
    _mapView.settings.myLocationButton = YES;
  // mapView.showsUserLocation = YES;

    _mapView.delegate = self;
 //   self.view = mapView;
   // [self.googleMapView addSubview:mapView];
    [_mapView setFrame:CGRectMake(0, _mapView.frame.origin.y, _mapView.frame.size.width, _startbutton.frame.origin.y-_mapView.frame.origin.y)];
    [_mapView addSubview:_MapSubeView];
    [_mapView addSubview:_table_View];
    [self placeMarkers];
    [self CreateOverlay];

    self.menuView.hidden = true;
    self.blurView.hidden = true;
    self.viewToShowWebViewForTermsNPrivacy.hidden = true;
    
    arrayForZoom = [[NSMutableArray alloc] initWithObjects:@"1 Km",@"5 Km",@"10 Km",@"20 Km", nil];
   
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)CreateOverlay{
    NSString *dataFile = [[NSBundle mainBundle] pathForResource:@"quake" ofType:@"plist"];
    NSArray *quakeData = [[NSArray alloc] initWithContentsOfFile:dataFile];
    NSLog(@"%@",quakeData);
    self.locations = [[NSMutableArray alloc] initWithCapacity:[quakeData count]];
    self.weights = [[NSMutableArray alloc] initWithCapacity:[quakeData count]];
    for (NSDictionary *reading in quakeData)
    {
        CLLocationDegrees latitude = [[reading objectForKey:kLatitude] doubleValue];
        CLLocationDegrees longitude = [[reading objectForKey:kLongitude] doubleValue];
        double magnitude = [[reading objectForKey:kMagnitude] doubleValue];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        [self.locations addObject:location];
        
        [self.weights addObject:[NSNumber numberWithInteger:(magnitude * 10)]];
    }
    
    // set map region
   // MapViewios.delegate = self;
  
    
    
    MKCoordinateSpan span = MKCoordinateSpanMake(10.0, 13.0);
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(39.0, -77.0);
    MapViewios.region = MKCoordinateRegionMake(center, span);
    
    // create overlay view for the heatmap image
    self.imageView = [[UIImageView alloc] initWithFrame:MapViewios.frame];
    self.imageView.contentMode = UIViewContentModeCenter;
    [self.view addSubview:self.imageView];
    
    float boost = 0.2;
    UIImage *heatmap = [LFHeatMap heatMapForMapView:MapViewios boost:boost locations:self.locations weights:self.weights];
    //self.imageView.image = heatmap;

    GMSGroundOverlay *groundOverlay = [GMSGroundOverlay groundOverlayWithPosition:CLLocationCoordinate2DMake(18.5204, 73.8567) icon:heatmap zoomLevel:5];
    groundOverlay.map=_mapView;

}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:
(NSArray *)locations
{
    NSLog(@"location info object=%@", [locations lastObject]);
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Cannot find the location.");
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    if ([keyPath isEqualToString:@"myLocation"]) {
        //            NSLog(@"My position changed");
    }
    
}
-(void)dealloc{
    [_mapView removeObserver:self forKeyPath:@"myLocation"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)placeMarkers
{
    GMSMarker *marker = [[GMSMarker alloc] init];
    
    marker.position = CLLocationCoordinate2DMake([Lat doubleValue], [Long doubleValue]);
    marker.title = @"PopUp HQ";
    marker.snippet = @"Durham, NC";
    marker.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
    marker.opacity = 0.9;
    marker.map = _mapView;
}
-(void)registerWebService{
    
    NSInteger success = 0;
    @try {
        NSString *Status=@"ACTIVE";
        //NSString *Register=@"sync";
        // FinalBaseString = [Base64String stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
        //NSString *bithrdate=@"15/08/1986";
        NSString *post =[[NSString alloc] initWithFormat:@"Status=%@&latitude=%@&longitude=%@",Status,Lat,Long];
        
        NSLog(@"hi...%@",post);
        // if ([registerTypeString isEqualToString:@"Grower"]) {
        NSURL  *url=[NSURL URLWithString:@"http://augdeck.com/Celebrity/service/lati_langi"];
        
        // }
        // else
        // {
        //url=[NSURL URLWithString:@"http://www.auctionfresh.com/AuctionFresh/webservisesios/AF_buyerRegister.php"];
        // }
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        
        // [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
        
        NSError *error =nil;
        NSHTTPURLResponse *response = nil;
        NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSLog(@"response%@",response);
        NSLog(@"Response code: %ld", (long)[response statusCode]);
        
        if ([response statusCode] >= 200 && [response statusCode] < 300)
        {
            NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
            NSLog(@"Response ==> %@", responseData);
            
            NSError *error = nil;
            //                NSDictionary *jsonData = [NSJSONSerialization
            //                                          JSONObjectWithData:urlData
            //                                          options:NSJSONReadingMutableContainers
            //                                          error:&error];
            NSMutableArray *topLevelArray = [NSJSONSerialization JSONObjectWithData:urlData
                                                                            options:NSJSONReadingMutableContainers error:&error];
            NSDictionary *jsonData = topLevelArray[0];
            //NSLog(@"rrrrr%@",jsonData);
            
            id object = topLevelArray[0];
            NSUInteger index = [topLevelArray indexOfObject:object];
            NSLog(@"index %lu",(unsigned long)index);
            //NSString *Uid;
            
            
            //NSLog(@"UID%@",Uid);
            
            
            success = [jsonData[@"success"] integerValue];
            NSLog(@"Success: %ld",(long)success);
            
            NSString *regrString=[[topLevelArray objectAtIndex:0] objectForKey:@"result"];
            NSLog(@"..%@",regrString);
            if (success == 2) {
                
                UIAlertController *alertController = [UIAlertController
                                                      alertControllerWithTitle:@"Hi"
                                                      message:@"OTP Incorrect"
                                                      preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alertController animated:YES completion:nil];
                
            }
            
            else if(success == 1)
            {
                [self performSegueWithIdentifier:@"BackToLogin" sender:self];
                UIAlertController *alertController1 = [UIAlertController
                                                       alertControllerWithTitle:@"Hello"
                                                       message:@"You have registered successfully !!!"
                                                       preferredStyle:UIAlertControllerStyleAlert];
                [self presentViewController:alertController1 animated:YES completion:nil];
                
                
                NSLog(@"Login SUCCESS");
                
                // [self SaveInDBGrower];
                
                // LoginViewController *log=[[LoginViewController alloc]init];
                // log.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
                
                // [self presentViewController:log animated:YES completion:Nil];
                //                if(success == 2){
                //
                //                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry"
                //                                                                        message:@"Email already Exist!!"
                //                                                                       delegate:self
                //                                                              cancelButtonTitle:@"Ok"
                //                                                              otherButtonTitles:nil, nil];
                //                    //alertView.tag = tag;
                //                    [alertView show];
                //
                //                }
                // NSString *error_msg = (NSString *) jsonData[@"error_message"];
                //   [self alertStatus:error_msg :@" Profile Register..!" :0];
                
                //successLbl.text=@"U are Logged in Successfully";
                
                //NSString *query;
                //if (self.recordIDToEdit == -1) {
                //query = [NSString stringWithFormat:@"insert into Users(Uname,password) values( '%@','%@')", [self.iCardID text],[self.password text] ];
                //[self.dbManager executeQuery:query];
                //if (self.dbManager.affectedRows != 0) {
                // NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                // }
                // else{
                // NSLog(@"Could not execute the query.");
                // }
                // NSString *que5=[NSString stringWithFormat:@"update Users set Uid=('%@')",Uid];
                //[self.dbManager executeQuery:que5];
                
            }
            
            // } else {
            
            //                NSString *error_msg = (NSString *) jsonData[@"error_message"];
            //                [self alertStatus:error_msg :@"Sign in Failed!" :0];
            //}
            
        } else {
            //if (error) NSLog(@"Error: %@", error);
            //[self alertStatus:@"Connection Failed" :@"Sign in Failed!" :0];
        }
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        //[self alertStatus:@"Sign in Failed." :@"Error!" :0];
    }
    if (success) {
        NSLog(@"login succeess");
    }
    // UserListViewController *user=[[UserListViewController alloc]init];
    //  navC=[[UINavigationController alloc]initWithRootViewController:user];
    
    // [self presentViewController:navC animated:YES completion:nil]
}
- (void) alertStatus:(NSString *)msg :(NSString *)title :(int) tag
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    alertView.tag = tag;
    [alertView show];
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)highlightLetter:(UITapGestureRecognizer*)sender {
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)menuButton:(UIButton *)sender
{
    self.menuView.hidden = false;
    self.blurView.hidden = false;
    [self.view bringSubviewToFront:_blurView];
    [self.view bringSubviewToFront:self.menuView];
//    [self.tabBarController.view bringSubviewToFront:_blurView];
    letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    [_blurView addGestureRecognizer:letterTapRecognizer];
}


- (IBAction)imprintAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    // self.tabBarController.tabBar.hidden = true;
    
    self.titleLabelTerms.text = @"IMPRINT";
    [self showWebView:@"https://bodyweight-connect.com/imprint.html"];
}

- (IBAction)termsAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    self.titleLabelTerms.text = @"TERMS";
    [self showWebView:@"https://bodyweight-connect.com/terms.html"];
}

- (IBAction)privacyAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    self.titleLabelTerms.text = @"PRIVACY";
    [self showWebView:@"https://bodyweight-connect.com/privacy.html"];
}

-(void) showWebView:(NSString *)urlStr
{
    self.viewToShowWebViewForTermsNPrivacy.hidden = false;
    NSURLRequest *newRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self.webViewTerms loadRequest:newRequest];
}

- (IBAction)backAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    
    [_blurView removeGestureRecognizer:letterTapRecognizer];
}

- (IBAction)settingsAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    SettingsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
    [self presentViewController:obj animated:true completion:nil];
}

- (IBAction)closeButton:(UIButton *)sender
{
    self.viewToShowWebViewForTermsNPrivacy.hidden = true;
}

- (IBAction)StartAction:(UIButton *)sender
{
    if(sender.tag == 100)
    {
        [_startbutton setTitle:@"STOP TRAINING" forState:UIControlStateNormal];
        _startbutton.tag = 101;
    
    } else  {
    
        [_startbutton setTitle:@"START TRAINING" forState:UIControlStateNormal];
        _startbutton.tag = 100;
        
    }
}

- (IBAction)zoomSelectionAction:(UIButton *)sender
{
    if(self.table_View.isHidden == true)
    {
    self.table_View.hidden = false;
    } else {
    
    self.table_View.hidden = true;
    
    }
    
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"cell1";
    MeetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.label1.text = arrayForZoom[indexPath.row];
    return cell;

    }
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *stringForTitle = arrayForZoom[indexPath.row];
    [self.zoomSelectButton setTitle:stringForTitle forState:UIControlStateNormal];
    self.table_View.hidden = true;
}
@end
