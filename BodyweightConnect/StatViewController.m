//
//  StatViewController.m
//  BodyweightConnect
//
//  Created by Augment Deck Technologies on 14/12/16.
//  Copyright © 2016 Augment Deck Technologies. All rights reserved.
//

#import "StatViewController.h"
#import "ImprintTermsPrivacy.h"
#import "SettingsVC.h"

@interface StatViewController ()


@end

@implementation StatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"STATS";
    NSString *url=@"http://bwcnnct.com/median/wo_median.php";
    NSURLRequest *newRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.webView loadRequest:newRequest];
    
    self.menuView.hidden = true;
    self.blurView.hidden = true;
    
    self.viewToShowWebViewForTermsNPrivacy.hidden = true;
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)highlightLetter:(UITapGestureRecognizer*)sender {
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)menuButton:(UIButton *)sender
{
    self.menuView.hidden = false;
    self.blurView.hidden = false;
    [self.view bringSubviewToFront:_blurView];
    [self.view bringSubviewToFront:self.menuView];
  
    letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    [_blurView addGestureRecognizer:letterTapRecognizer];
}

- (IBAction)imprintAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
   // self.tabBarController.tabBar.hidden = true;
    
    self.titleLabelTerms.text = @"IMPRINT";
    [self showWebView:@"https://bodyweight-connect.com/imprint.html"];
}

- (IBAction)termsAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    self.titleLabelTerms.text = @"TERMS";
    [self showWebView:@"https://bodyweight-connect.com/terms.html"];
}

- (IBAction)privacyAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    self.titleLabelTerms.text = @"PRIVACY";
    [self showWebView:@"https://bodyweight-connect.com/privacy.html"];
}

-(void) showWebView:(NSString *)urlStr
{
    self.viewToShowWebViewForTermsNPrivacy.hidden = false;
    NSURLRequest *newRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self.webViewTerms loadRequest:newRequest];
}

- (IBAction)backAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
}

- (IBAction)settingsAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    SettingsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
    [self presentViewController:obj animated:true completion:nil];
}

- (IBAction)closeButton:(UIButton *)sender
{
    self.viewToShowWebViewForTermsNPrivacy.hidden = true;
}
@end
