//
//  AppDelegate.h
//  BodyweightConnect
//
//  Created by Augment Deck Technologies on 14/12/16.
//  Copyright © 2016 Augment Deck Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

