//
//  MeetViewController.h
//  BodyweightConnect
//
//  Created by Augment Deck Technologies on 14/12/16.
//  Copyright © 2016 Augment Deck Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface MeetViewController : UIViewController<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
  //  GMSMapView *mapView;
    CLLocation *currentLocation;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    
    UITapGestureRecognizer *letterTapRecognizer;
    
    NSMutableArray *arrayForZoom;
}
@property (weak, nonatomic) IBOutlet MKMapView *MapViewios;
@property(nonatomic,retain)NSString *Lat;
@property(nonatomic,retain)NSString *Long;

@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *googleMapView;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) IBOutlet UIView *menuView;
@property (strong, nonatomic) IBOutlet UIView *blurView;

@property (strong, nonatomic) IBOutlet UIView *viewToShowWebViewForTermsNPrivacy;
@property (strong, nonatomic) IBOutlet UIWebView *webViewTerms;
@property (strong, nonatomic) IBOutlet UILabel *titleLabelTerms;
@property (strong, nonatomic) IBOutlet UIButton *startbutton;
@property (strong, nonatomic) IBOutlet UIView *MapSubeView;
@property (strong, nonatomic) IBOutlet UITableView *table_View;
@property (strong, nonatomic) IBOutlet UIButton *zoomSelectButton;

- (IBAction)imprintAction:(UIButton *)sender;
- (IBAction)termsAction:(UIButton *)sender;
- (IBAction)privacyAction:(UIButton *)sender;
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)settingsAction:(UIButton *)sender;
- (IBAction)closeButton:(UIButton *)sender;
- (IBAction)StartAction:(UIButton *)sender;
- (IBAction)zoomSelectionAction:(UIButton *)sender;

@end
