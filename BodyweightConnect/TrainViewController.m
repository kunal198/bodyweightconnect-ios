//
//  TrainViewController.m
//  BodyweightConnect
//
//  Created by Augment Deck Technologies on 14/12/16.
//  Copyright © 2016 Augment Deck Technologies. All rights reserved.
//

#import "TrainViewController.h"
#import "SettingsVC.h"
#import "SportsTableViewcell.h"

@interface TrainViewController ()<UITextViewDelegate, UIWebViewDelegate>

@end

@implementation TrainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"TRAIN";
    
//    self.webViewTerms.delegate = self;
//    self.webViewTerms.scalesPageToFit = YES;

    double screenHeight = [[UIScreen mainScreen] bounds].size.height;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
    {
        NSLog(@"All iPads");
    } else if (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone)
    {
        if(screenHeight == 480) {
            NSLog(@"iPhone 4/4S");
           
        } else if (screenHeight == 568) {
            NSLog(@"iPhone 5/5S/SE");
          
        } else if (screenHeight == 667) {
            NSLog(@"iPhone 6/6S");
        } else if (screenHeight == 736) {
            NSLog(@"iPhone 6+, 6S+");
            
            _demolabel.font=[_demolabel.font fontWithSize:16];
            [_flicImage setFrame:CGRectMake(28, _flicImage.frame.origin.y, _flicImage.frame.size.width, _flicImage.frame.size.height)];
            [_demotextview setFrame:CGRectMake(115, _demotextview.frame.origin.y, _demotextview.frame.size.width, _demotextview.frame.size.height)];
            
        } else {
            NSLog(@"Others");
        }
    }
    // Do any additional setup after loading the view.
    
    self.menuView.hidden = true;
    self.blurView.hidden = true;
    self.viewToShowWebViewForTermsNPrivacy.hidden = true;
    //[self demofunc];
    
    NSString *str = @"Order your flic for faster workouts.";
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:str];
    [text addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:[str rangeOfString:@"Order your flic"]];
    [text addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:105/255.0 green:173/255.0 blue:4/255.0 alpha:1.0] range:[str rangeOfString:@"Order your flic"]];
    [text addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15.0] range:[str rangeOfString:@"Order your flic for faster workouts."]];
    _demotextview.attributedText = text;
    
//    NSLog(@"%f",self.sportsTableView.frame.size.height);
//    NSLog(@"%f",self.sportsTableView.frame.size.height / 4);
//    self.sportsTableView.estimatedRowHeight = 30;
//    self.sportsTableView.rowHeight = UITableViewAutomaticDimension;
//
//    [self.sportsTableView reloadData];
}

//- (void)webViewDidFinishLoad:(UIWebView *)theWebView
//{
//    CGSize contentSize = theWebView.scrollView.contentSize;
//    CGSize viewSize = theWebView.bounds.size;
//    
//    float rw = viewSize.width / contentSize.width;
//    
//    theWebView.scrollView.minimumZoomScale = rw;
//    theWebView.scrollView.maximumZoomScale = rw;
//    theWebView.scrollView.zoomScale = 0.5;
//}
//

-(void)demofunc
{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"Order your flic for faster workouts."];
    [attributedString addAttribute:NSLinkAttributeName
                             value:@"username://marcelofabri_"
                             range:[[attributedString string] rangeOfString:@"Order your flic"]];
    
    NSDictionary *linkAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:105/255 green:173/255 blue:4/255 alpha:1.0],
                                     NSUnderlineColorAttributeName: [UIColor colorWithRed:105/255 green:173/255 blue:4/255 alpha:1.0],
                                     NSUnderlineStyleAttributeName: @(NSUnderlineStyleThick)};
    
    // assume that textView is a UITextView previously created (either by code or Interface Builder)
    self.demotextview.linkTextAttributes = linkAttributes; // customizes the appearance of links
    self.demotextview.attributedText = attributedString;
    self.demotextview.delegate = self;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)highlightLetter:(UITapGestureRecognizer*)sender {
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)menuButton:(UIButton *)sender
{
    self.menuView.hidden = false;
    self.blurView.hidden = false;
    [self.view bringSubviewToFront:_blurView];
    [self.view bringSubviewToFront:self.menuView];
    
    letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    [_blurView addGestureRecognizer:letterTapRecognizer];
}


- (IBAction)imprintAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    // self.tabBarController.tabBar.hidden = true;
    
    self.titleLabelTerms.text = @"IMPRINT";
    [self showWebView:@"https://bodyweight-connect.com/imprint.html"];
}

- (IBAction)termsAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    self.titleLabelTerms.text = @"TERMS";
    [self showWebView:@"https://bodyweight-connect.com/terms.html"];
}

- (IBAction)privacyAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    self.titleLabelTerms.text = @"PRIVACY";
    [self showWebView:@"https://bodyweight-connect.com/privacy.html"];
}

-(void) showWebView:(NSString *)urlStr
{
    self.viewToShowWebViewForTermsNPrivacy.hidden = false;
    NSURLRequest *newRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self.webViewTerms loadRequest:newRequest];
}

- (IBAction)backAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
}

- (IBAction)settingsAction:(UIButton *)sender
{
    _menuView.hidden = true;
    _blurView.hidden = true;
    [_blurView removeGestureRecognizer:letterTapRecognizer];
    
    SettingsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
    [self presentViewController:obj animated:true completion:nil];
}

- (IBAction)closeButton:(UIButton *)sender
{
    self.viewToShowWebViewForTermsNPrivacy.hidden = true;
}

- (IBAction)AddPageAction:(UIButton *)sender
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Add Page"
                                                                              message: @"Fill Name and Url to add Page"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Name";
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Url";
        textField.text = @"https://";
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
       UITextField * namefield = textfields[0];
        UITextField * passwordfiled = textfields[1];
        NSLog(@"%@:%@",namefield.text,passwordfiled.text);
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)phoneSelectionAction:(UIButton *)sender
{
    if(sender.tag == 100)
    {
      
    } else {
        _flicButton.tag = 200;
        _phonebutton.tag = 100;
        [_phonebutton setImage:[UIImage imageNamed:@"RadioButtonselected"] forState:UIControlStateNormal];
        [_flicButton setImage:[UIImage imageNamed:@"RadioButtonUnselected"] forState:UIControlStateNormal];
    }
    
}

- (IBAction)flicSelectionAction:(UIButton *)sender
{

   
    
    if(sender.tag == 200)
    {
        self.phonebutton.tag = 101;
        self.flicButton.tag = 201;
        [_flicButton setImage:[UIImage imageNamed:@"RadioButtonselected"] forState:UIControlStateNormal];
        [_phonebutton setImage:[UIImage imageNamed:@"RadioButtonUnselected"] forState:UIControlStateNormal];
    } else {
        
       

        
    }

}

#pragma mark TableView Delegate And DataSources

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"cell1";
    SportsTableViewcell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.label1.text = @"abcd";
    [cell.button1 setImage:[UIImage imageNamed:@"RadioButtonselected"] forState:UIControlStateNormal];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // NSString *stringForTitle = arrayForZoom[indexPath.row];
   //[self.zoomSelectButton setTitle:stringForTitle forState:UIControlStateNormal];
   // self.table_View.hidden = true;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80; // customize the height
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
@end
