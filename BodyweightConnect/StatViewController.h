//
//  StatViewController.h
//  BodyweightConnect
//
//  Created by Augment Deck Technologies on 14/12/16.
//  Copyright © 2016 Augment Deck Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatViewController : UIViewController
{
    UITapGestureRecognizer *letterTapRecognizer;
    
}
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIView *menuView;
@property (strong, nonatomic) IBOutlet UIView *blurView;
@property (strong, nonatomic) IBOutlet UIView *viewToShowWebViewForTermsNPrivacy;
@property (strong, nonatomic) IBOutlet UIWebView *webViewTerms;
@property (strong, nonatomic) IBOutlet UILabel *titleLabelTerms;
- (IBAction)menuButton:(UIButton *)sender;
- (IBAction)imprintAction:(UIButton *)sender;
- (IBAction)termsAction:(UIButton *)sender;
- (IBAction)privacyAction:(UIButton *)sender;
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)settingsAction:(UIButton *)sender;
- (IBAction)closeButton:(UIButton *)sender;

@end
